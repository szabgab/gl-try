# Try GitLab

* Line 1
* Line 2


* How to pass parameter to a CI job
     - In the Web US we can do this.
     - In the scheduled jobs we can set these up.

* How to require parameters


* How to disable running a job when we push
    only:
      variables:
        - $CI_PIPELINE_SOURCE != "push"


https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-advanced

* Can I set environment variable in scheduled jobs.
    - yes, I can
     https://docs.gitlab.com/ee/ci/pipelines/schedules.html

* Can I run differnet part of a job based on environment variables.
    - yes, but probably it is better to have separate repos all include the jobs from a central repo.

web tests are in the   'web-tests' repo and the .gitlab-ci.yml includes the
    include:
      - project: szabgab/gl-try
        file: ci/web-tests.yml

database tests are in the   'database-tests' repo and the .gitlab-ci.yml includes the
    include:
      - project: szabgab/gl-try
        file: ci/database-tests.yml

* How to run a job via the API?

CI_PIPELINE_SOURCE=trigger
curl -X POST  -F token=6bf82401fedaf78342d1a36f142953 -F "variables[EXPECTED_PARAM]=curl"  -F ref=master https://gitlab.com/api/v4/projects/7386475/trigger/pipeline

Returns 404 in case the token is invalid.
{"message":"404 Not Found"}

https://docs.gitlab.com/ee/ci/triggers/


