#FROM alpine:latest
#WORKDIR /opt
#ARG PASS_WORD
#RUN date > date.txt
#RUN printenv | sort > printenv.txt
#RUN echo $PASS_WORD >> date.txt
#RUN echo "done" >> date.txt


#FROM python:3.9
#WORKDIR /opt
#RUN pip install flask
#COPY requirements-dev.txt .
#RUN pip install -r requirements-dev.txt

FROM alpine:latest
RUN apk add perl
RUN apk add python3 py3-pip
RUN python3 --version
RUN pip install flask
WORKDIR /opt
COPY app.py .
RUN date > dev.txt
#EXPOSE 5000
CMD ["flask", "run", "--host", "0.0.0.0"]
#COPY requirements.txt .
#RUN pip install -r requirements.txt
