from flask import Flask, abort
import datetime

app = Flask(__name__)

app.logger.info("when the server launches")

@app.before_first_request
def before_first_request():
    app.logger.info("before_first_request")

@app.before_request
def before_request():
    app.logger.info("before_request")


@app.after_request
def after_request(response):
    app.logger.info("after_request")
    return response

@app.teardown_request
def teardown_request(err=None):
    app.logger.info("teardown_request")
    if err is not None:
        app.log.error(err)

@app.errorhandler(500)
def server_error(e):
    return "500"

@app.errorhandler(Exception)
def server_error(e):
    app.logger.exception(e)
    return "exception"

@app.errorhandler(ZeroDivisionError)
def server_error(e):
    app.logger.exception(e)
    return "Zero division"

@app.route("/")
def main():
    app.logger.info("main route")
    return "Hello " + str(datetime.datetime.now())

@app.route("/crash")
def crash():
    app.logger.info("crash route")
    a = 0
    b = 3 / a
    raise Exception("Oups")
    abort(500)
