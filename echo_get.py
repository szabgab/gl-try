from flask import Flask, request
app = Flask(__name__)

VERSION = "1.02"

@app.route("/")
def main():
    return f'''
     hello
     VERSION {VERSION}<br>
     <form action="/echo" method="GET">
         <input name="text">
         <input type="submit" value="Echo">
     </form>
     '''

@app.route("/echo")
def echo():
    text = request.args.get('text', '')
    return "Why You said: " + text
