
Get-Host | Select-Object Version
Get-Item Env:*
dir
if ( -not (Test-Path -Path 'strawberry' -PathType Container ) ) {
    Invoke-WebRequest -Uri http://strawberryperl.com/download/5.32.0.1/strawberry-perl-5.32.0.1-64bit-portable.zip -UseBasicParsing -Outfile strawberry.zip
    dir
    Expand-Archive -Path strawberry.zip
    dir
}
strawberry\perl\bin\perl -V
