from flask import Flask, request
import time
import os

app = Flask(__name__)


data_dir = "/internal-demo/"
output_file = os.path.join(data_dir, 'output.txt')

@app.route("/")
def hello():
    return '''
    Send a number for processing
    <form action="/submit" method="GET"><input name="text"><input type="submit" value="Submit"></form>
    <a href="/results">results</a>
    '''

@app.route("/submit")
def submit_data():
    from_user = request.args.get('text')
    filename = f"input_{time.time()}"
    if from_user:
        with open(filename, 'w') as fh:
            fh.write(from_user)
        return 'Saved'
    return 'Value missing'

@app.route("/results")
def get_results():
    if os.path.exists(output_file):
        with open(output_file) as fh:
            output = fh.read()
        return f'<h2>Output</h2><pre>{output}</pre>'
    return 'No output yet'

