const add = require('../define_function.js');

//console.log(add(2, 3))

test('adds 1 + 2 to equal 3', () => {
  expect(add(1, 2)).toBe(3);
});