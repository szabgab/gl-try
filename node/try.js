



function hi() {
    console.log("hello");
}

//hi();
//setInterval(hi, 1000);
//setTimeout(hi, 1000);
//setTimeout(hi(), 1000);

function runner(cb) {
    console.log("start");
    cb(23);
    console.log("end");
}


function hello(x) {
    console.log(x);
}


function hello_boris(x) {
    console.log("Boris");
}

//hello(17);
//var qqrq = hello;
//qqrq(78);
//runner(hello);
//runner(hello_boris);

function abc (x) {
    console.log("on the fly ", x);
}

runner(abc);
