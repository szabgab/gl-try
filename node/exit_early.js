console.log(process.argv.length)


if (process.argv.length < 3) {
    console.log("exiting early")
    process.exit(1);
}

console.log("exiting normally")

// if you run this program as node exit_early.js it will print "exiting early"
// if you pass a paramerer on the command line "node exit_early.js 42" then it will print "exiting normall"
